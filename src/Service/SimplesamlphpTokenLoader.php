<?php
/**
 * Parses the assertion to extract the token.
 * @author Andrea Mallegni, T.A.I. srl
 * @version $Id: TokenLoader.class.php 850 2010-06-08 08:16:42Z amallegni $
 */

namespace Drupal\simplesamlphp_auth\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use SimpleSAML\Auth\Simple;
use SimpleSAML_Configuration;
use Drupal\simplesamlphp_auth\Exception\SimplesamlphpAttributeException;
use Drupal\Core\Site\Settings;

class SimplesamlphpTokenLoader {
	/**
	 * Parses the assertion for the session token and returns the result as a
	 * <code>SpAgentArpaSSOTokenBean</code> object.
	 */
	static function loadToken($xml) {
		$token = self::parseTokenFromXmlString($xml);
		return $token;
	}

	static function simpleXmlToArray($xmlObject) {
		$array = [];
		foreach ($xmlObject->children() as $node) {
			\Drupal::logger('simpleXMLToArray')->debug($node->getName());

			$array[$node->getName()] = is_array($node) ? self::simplexml_to_array($node) : (string) $node;
		}
		return $array;
	}

	static function xml2array($xmlObject, $out = array ()) {
		foreach ((array)$xmlObject as $index => $node )
		{
			$out[$index] = (is_object ( $node )) ? self::xml2array($node) : $node;
		}
		return $out;
	}
	/**
	 * Parses the assertion and returns a <code>SpAgentArpaSSOTokenBean</code> object.
	 * @param $xmlString the arpaSSOToken xml message in the assertion
	 */
	static private function parseTokenFromXmlString(&$xmlString) {
		$xml = null;
		if(strpos($xmlString, "<?xml version='1.0' encoding='ISO-8859-1'?>") === false) {
			$xmlString = "<?xml version='1.0' encoding='ISO-8859-1'?>"
			. html_entity_decode($xmlString);
		}
		$xmlString = str_replace('xmlns=', 'ns=', $xmlString);
		try {
			$xml = simplexml_load_string($xmlString);
		} catch (Exception $e) {
			print "ERROR PARSING XML: " . $e;
			return null;
		}

		return $xml;
		//$authenticatedUser = self::parseUser($xml, false);
		//$delegatingUser = self::parseUser($xml, true);
		//$workingRoles = self::parseWorkingRoles($xml);
		//$attributes = self::parseSSOTokenAttributes($xml);
		//$lastUpdate = $xml->ssotoken->lastUpdate;

		//$token = new SpAgentArpaSSOTokenBean($authenticatedUser,
		//$delegatingUser, $workingRoles);
		//$token->setLastUpdate($lastUpdate);
		//$token->setAttributes($attributes);

		//return $token;
	}
	/**
	 * Parses user from assertion. If $delegating is setted to
	 * true the delegated user will be parsed instead of the
	 * authenticated user.
	 * @param  $xml simpleXML object representing the assertion
	 * @param  $delegating true | false
	 */
	static private function parseUser(&$xml, $delegating = false) {
		if($delegating == false) {
			$base = $xml->ssotoken->user;
		} else {
			$base = $xml->ssotoken->delegatingUser;
		}

		$cf = strval($base->cf);

		if($cf != null) {
			$name = null;
			$surname = null;
			$email = null;
			$birthday = null;
			$birthcc = null;
			foreach($base->attribute as $arpa_attribute) {
				switch($arpa_attribute->name) {
					case "name":
						$name = strval($arpa_attribute->value);
						break;
					case "surname":
						$surname = strval($arpa_attribute->value);
						break;
					case "email":
						$email = strval($arpa_attribute->value);
						break;
					case "birthday":
						$birthday = strval($arpa_attribute->value);
						break;
					case "birthcc":
						$birthcc = strval($arpa_attribute->value);
						break;
				}
			}
			$authUser = new SpAgentArpaUser($cf, $name, $surname, $email, $birthday, $birthcc);
			return $authUser;
		} else {
			return null;
		}
	}
	/**
	 * Parses working roles calling
	 * <code>self::parseRole(&$role, &$roleParent = null)</code>.
	 * @param  $xml simpleXML object representing the xml message in the assertion
	 */
	static private function parseWorkingRoles(&$xml) {
		$roleTree = array();
		$workingRoles = array();
		foreach($xml->ssotoken->workingRole as $workingRole) {
			$roleTree[] = self::parseRole($workingRole);
		}
		foreach ($roleTree as $key => $role) {
			$workingRoles[] = $role;
			self::addChildrenToWorkingRoles($workingRoles, $role);
		}
		return $workingRoles;
	}
	/**
	 * Completes the working roles array with all roles
	 * in the role tree. This could be done while parsing children of role,
	 * but in this way informations about parent of children roles would get lost.
	 * @param  $workingRoles
	 * @param  $role
	 */
	static private function addChildrenToWorkingRoles(&$workingRoles, &$role) {
		$children = $role->getChildren();
		if (is_array($children)) {
			foreach ($children as $key => $child) {
				if (!in_array($child, $workingRoles, true)) {
					$workingRoles[] = $child;
					self::addChildrenToWorkingRoles($workingRoles, $child);
				}
			}
		}
	}

	/**
	 * Parses a working role from assertion.
	 * @param  $role simpleXML object representing the role to parse
	 * @param  $roleParent parent ArpaRole
	 */
	static private function parseRole(&$role) {
		$roleName = $role->name;
		$roleChildren = self::parseChildren($role);
		$parsedRole = new SpAgentArpaRole();
		$parsedRole->setName(strval($roleName));
		$parsedRole->addChildren($roleChildren); //parent is setted inside this call
		$parsedRole->addLocalAttributes(self::parseRoleAttributes($role));
		return $parsedRole;
	}
	/**
	 * Parse attributes of supplied role.
	 * @param  $roleParent simpleXML object representing the role to
	 * 			be parsed for attributes
	 */
	static private function parseRoleAttributes(&$role) {
		$localAttrs = array();
		foreach($role->attribute as $attr) {
			$values = array();
			foreach ($attr->value as $attribute_value) {
				$values[] = strval($attribute_value);
			}
			$localAttrs[] =
			new SpAgentArpaAttribute(strval($attr->name), null, $values);
		}
		return $localAttrs;
	}


	/**
	 * Parse children roles of supplied parent role.
	 * @param  $roleParent simpleXML object representing the role to
	 * 			be parsed for children
	 */
	static private function parseChildren(&$roleParent) {
		$children = array();
		foreach($roleParent->child as $child) {
			$children[] = self::parseRole($child);
		}
		return $children;
	}
	/**
	 * Parses the ssoToken attributes from assertion.
	 * @param  $xml simpleXML object representing the xml message in the assertion
	 */
	static private function parseSSOTokenAttributes(&$xml) {
		$attributes = array();
		foreach($xml->ssotoken->attribute as $attribute) {
			$attributes[] =
			new ArpaSSOTokenAttribute(strval($attribute->name),
			$attribute->value);
		}
		return $attributes;
	}

}
?>
